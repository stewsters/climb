package com.stewsters.climb.worldBuilder

import com.stewsters.climb.tile.TileType
import com.stewsters.climb.tile.World
import com.stewsters.util.math.MatUtils
import com.stewsters.util.math.geom.Rect
import com.stewsters.util.math.geom.RectPrism
import com.stewsters.util.math.geom.RectSubdivider

public class RandomCityChunkBuilder {


    public static final groundHeight = 4;

    private int width;
    private int height;
    private int depth;
    private TileType[][][] tiles = null;

    public RandomCityChunkBuilder(int width, int height, int depth) {
        this.width = width
        this.height = height
        this.depth = depth
        this.tiles = new TileType[width][height][depth]
    }

    public World build() {
        return new World(tiles);
    }


    private RandomCityChunkBuilder flattenWorld() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {

                    tiles[x][y][z] = z <= groundHeight ? TileType.SAND : TileType.AIR;
                }
            }
        }
        return this;
    }


    public RandomCityChunkBuilder constructBuildings(int minSize) {

        //check average height for this

        // make a lot representing the whole, except for the street
        Rect whole = new Rect(2, 2, width - 3, height - 3)


        List<Rect> lots = RectSubdivider.divide(whole, minSize)

        lots.each { lot ->
            // height in floors
            int totalFloors = MatUtils.getIntInRange(1, 5)

            // This gives you the separation level around
            int extendedWalk = MatUtils.getIntInRange(0, 2)

            Rect foundation = new Rect(
                    lot.x1 + extendedWalk,
                    lot.y1 + extendedWalk,
                    lot.x2 - extendedWalk,
                    lot.y2 - extendedWalk)

            (0..totalFloors).each { floor ->

                wall(foundation, groundHeight + (floor * 2) + 1, TileType.WALL)
                ceiling(foundation, groundHeight + (floor * 2) + 2, TileType.WALL)

            }


        }

        // make foundations
        return this;

    }

    private void fill(RectPrism prism, TileType tileType) {

        for (int x = prism.x1; x <= prism.x2; x++) {
            for (int y = prism.y1; y <= prism.y2; y++) {
                for (int z = prism.z1; z <= prism.z2; z++) {

                    tiles[x][y][z] = tileType;
                }
            }
        }
    }

    private void ceiling(Rect prism, int z, TileType tileType) {

        for (int x = prism.x1; x <= prism.x2; x++) {
            for (int y = prism.y1; y <= prism.y2; y++) {

                tiles[x][y][z] = tileType;
            }
        }
    }

    private void wall(Rect prism, int z, TileType tileType) {

        for (int x = prism.x1; x <= prism.x2; x++) {
            for (int y = prism.y1; y <= prism.y2; y++) {

                if ((x == prism.x1 || x == prism.x2) ||
                        (y == prism.y1 || y == prism.y2))
                    tiles[x][y][z] = tileType;
            }
        }
    }


}