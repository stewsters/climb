package com.stewsters.climb.creature

import com.stewsters.climb.creature.ai.FungusAi
import com.stewsters.climb.creature.ai.PlayerAi
import com.stewsters.climb.creature.ai.SoldierAi
import com.stewsters.climb.tile.World
import squidpony.squidcolor.SColor


public class CreatureFactory {
    private World world;

    public CreatureFactory(World world) {
        this.world = world;
    }

    public Creature newPlayer() {
        Creature player = new Creature(world: world, glyph: '@', color: SColor.WHITE);
        world.addAtEmptyLocation(player);
        new PlayerAi(player);
        return player;
    }

    public Creature newEnemy() {
        Creature soldier = new Creature(world: world, glyph: 's', color: SColor.GREEN);
        world.addAtEmptyLocation(soldier);
        new SoldierAi(soldier);
        return soldier;
    }


}
