Climb
======
A roguelike engine prototype for climbing.

A bit like Crawl, but with more height.


My tools:

* Java 8
* Groovy 2.3.3
* SquidLib 1.95.1 ( https://github.com/SquidPony/SquidLib)
* Intellij

Compile
=======
To compile from source you will need Java 7 and to install Groovy and Gradle.  If you use Linux or OSX, I recommend using http://gvmtool.net/ for that.

Once you are set up, it will be something like:

```bash
git clone git://github.com/stewsters/climb.git

cd climb

gradle run
```

Controls
========
```
Movement: numpad, vi keys, or arrow keys ( if you don't like diagonal movement )

climb down: <
climb up: >

Melee: Bump into enemies

```


Make a Binary
========

If you want to make a binary distribution of this (jar) you can do so by

```bash
gradle distZip
cd build/libs
java -jar climb-1.1.jar
```

(or double click on it in Windows)
